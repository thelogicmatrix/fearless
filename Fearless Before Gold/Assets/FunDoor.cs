﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class FunDoor : MonoBehaviour
{
    public GameObject tx, txx;
    public bool doorunlocked = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (doorunlocked == true)
        {
            tx.SetActive(false);
            txx.SetActive(true);
        }
        else if (doorunlocked == false)
        {
            tx.SetActive(true);
            txx.SetActive(false);
        }
    }
}

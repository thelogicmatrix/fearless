﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Epilogue : MonoBehaviour
{

    public GameObject darkv;
    public GameObject dimv;
    public GameObject brightv;
    public GameObject flash;


    // Update is called once per frame
    void Start()
    {

        StartCoroutine("Ending");


    }



    IEnumerator Ending()
    {
        yield return new WaitForSeconds(1.0f);
        darkv.SetActive(false);
        flash.SetActive(true);
        yield return new WaitForSeconds(1.0f);
        dimv.SetActive(true);
        flash.SetActive(false);
        yield return new WaitForSeconds(1.0f);
        flash.SetActive(true);
        dimv.SetActive(false);
        yield return new WaitForSeconds(1.0f);
        flash.SetActive(false);
        brightv.SetActive(true);
        yield return new WaitForSeconds(1.0f);
        print("hi");
        SceneManager.LoadScene("Main Menu");
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitiateWords3 : MonoBehaviour
{
    public GameObject txx, txxx, txxxx, txxxxx, txxxxxx, txxxxxxx, txxxxxxxx, txxxxxxxxx, tx100;
    public Rigidbody2D Rb;
    public GameObject player;
    public bool frozen;
    private playercontrols pc;
    public Animator Anim;
    // Start is called before the first frame update
    void Start()
    {
        Rb = player.GetComponent<Rigidbody2D>();
        pc = FindObjectOfType<playercontrols>();
        Anim = player.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (pc.holdingkeyinhand)
        {
            txx.SetActive(false);
             txxx.SetActive(false);
            txxxx.SetActive(false);
             txxxxxxx.SetActive(false);
             txxxxxxxx.SetActive(false);
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            StartCoroutine("Talk");
            if (!frozen)
            {
                Rb.constraints = RigidbodyConstraints2D.FreezeAll;
                StartCoroutine("Spine");
                frozen = true;
                if (!pc.holdingtorchinhand)
                {
                    Anim.CrossFade("Idle without crystal", 0f);
                }
                else
                {
                    Anim.CrossFade("idle with crystal", 0f);
                }
            }
        }
    }
    IEnumerator Spine()
    {
        yield return new WaitForSeconds(2.5f);
        Rb.constraints = RigidbodyConstraints2D.None;
        Anim.enabled = true;
    }
    IEnumerator Talk()
    {
        tx100.SetActive(true);
        txxxxx.SetActive(false);
        txxxxxx.SetActive(true);
        txx.SetActive(true);
        yield return new WaitForSeconds(0.15f);
        Anim.enabled = false;
        yield return new WaitForSeconds(0.35f);
        txxxxxx.SetActive(false);
        txxxxxxxxx.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        txxx.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        txxxxxxx.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        txxxxxxxx.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        frozen = false;
        // tx.SetActive(false);
        //txx.SetActive(false);
        // txxx.SetActive(false);
        txxxx.SetActive(false);
       // txxxxxxx.SetActive(false);
       // txxxxxxxx.SetActive(false);

    }
}
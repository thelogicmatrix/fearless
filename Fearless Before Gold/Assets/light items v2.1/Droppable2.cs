﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Droppable2 : MonoBehaviour
{
    //public GameObject Torch;
    public GameObject player, temporaryHome;
    //public Vector3 Offset;
    public bool ImUrs1, dropDelay;
    public Transform lightradius, lightradius2, lightradius3;
    private keybehaviour kb;
    private playercontrols pc;
    private Droppable dp1;
    private Droppable3 dp3;
    private Droppable4 dp4;
    private Droppable5 dp5;
    private Droppable6 dp6;
    private DroppableX dp7;

    //float nextPossibleInteraction = 0f;
    // Start is called before the first frame update
    void Start()
    {
        kb = FindObjectOfType<keybehaviour>();
        pc = FindObjectOfType<playercontrols>();
        dp1 = FindObjectOfType<Droppable>();
        dp3 = FindObjectOfType<Droppable3>();
        dp4 = FindObjectOfType<Droppable4>();
        dp5 = FindObjectOfType<Droppable5>();
        dp6 = FindObjectOfType<Droppable6>();
        dp7 = FindObjectOfType<DroppableX>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            ImUrs1 = true;
        }
        if (other.tag == "Ground")
        {
            Destroy(GetComponent<Rigidbody2D>());
            print("boop");
        }
    }
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            ImUrs1 = true;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            ImUrs1 = false;
        }
    }


    // Update is called once per frame
    public void Update()
    {

        //if (nextPossibleInteraction > Time.time) return;

        if (CrossPlatformInputManager.GetButtonDown("Drop") && pc.isGrounded)
        {
            if (pc.holdingtorchinhand2 && pc.PlayerUnlit) //touching unlit lamp
            {
                print("Light the Lamp");
                transform.parent = null;
                transform.parent = temporaryHome.transform; //transfer object to lamp
                pc.PlayerUnlit = false;
                pc.ifLitLampTouch = true;
                pc.awaken = true; //light lamp
                StartCoroutine("Stop");
            }
            else if (pc.holdingtorchinhand2 && !pc.ifLitLampTouch)
            {
                print("Drop Torch");
                transform.parent = null; //detach from parent
                gameObject.AddComponent(typeof(Rigidbody2D)); //drop
                                                              //ImUrs = false;
            }
            else if (ImUrs1 && !pc.holdingtorchinhand && !dp1.ImUrs && !dp3.ImUrs2 && !dp4.ImUrs3 && !dp5.ImUrs4 && !dp6.ImUrs5 && !dp7.ImUrs6 && !kb.keytouching && !pc.holdingkeyinhand && pc.ifLitLampTouch)
            {
                print("Take Out Torch");
                transform.parent = null;
                transform.parent = player.transform; //transfer object to player
                transform.position = player.transform.position; //set position to player's position
                pc.ifLitLampTouch = false;
                pc.PlayerUnlit = true;
                pc.sleep = true; //unlight lamp
                StartCoroutine("Stopped");
            }
            else if (ImUrs1 && !pc.holdingtorchinhand && !dp1.ImUrs && !dp3.ImUrs2 && !dp4.ImUrs3 && !dp5.ImUrs4 && !dp6.ImUrs5 && !dp7.ImUrs6 && !kb.keytouching && !pc.holdingkeyinhand && !dropDelay && !pc.ifLitLampTouch)
            {
                print("Pick Up Torch");
                transform.parent = player.transform; //transfer object to player
                transform.position = player.transform.position; //set position to player's position
                dropDelay = true; //delay repeated pick ups
                StartCoroutine("PickUpDelay");
            }

        }

        if (pc.holdingtorchinhand2)
            {
            ImUrs1 = true;
            GetComponent<Renderer>().sortingOrder = -3;
                lightradius.GetComponent<Renderer>().sortingOrder = 2;
            lightradius2.GetComponent<Renderer>().sortingOrder = 2;
            lightradius3.GetComponent<Renderer>().sortingOrder = 2;
        }
   else if (transform.parent == null)
        {
            GetComponent<Renderer>().sortingOrder = 2;
            lightradius.GetComponent<Renderer>().sortingOrder = 2;
            lightradius2.GetComponent<Renderer>().sortingOrder = 2;
            lightradius3.GetComponent<Renderer>().sortingOrder = 2;
        }
        else
        {
            GetComponent<Renderer>().sortingOrder = -3;
            lightradius.GetComponent<Renderer>().sortingOrder = -3;
            lightradius2.GetComponent<Renderer>().sortingOrder = -3;
            lightradius3.GetComponent<Renderer>().sortingOrder = -3;
        }

    }
    IEnumerator Stop()
    {
        yield return new WaitForSeconds(0.01f);
        pc.awaken = false;
    }
    IEnumerator Stopped()
    {
        yield return new WaitForSeconds(0.01f);
        pc.sleep = false;
    }
    IEnumerator PickUpDelay()
    {
        yield return new WaitForSeconds(1f);
        dropDelay = false;
    }
}

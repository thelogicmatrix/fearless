﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    public GameObject UItutorial;
    public GameObject NormalUI;
    public GameObject sensor;

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            print("tut");
            NormalUI.SetActive(false);
            UItutorial.SetActive(true);
            sensor.SetActive(false);
        }
    }



}

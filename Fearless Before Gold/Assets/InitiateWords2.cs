﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitiateWords2 : MonoBehaviour
{
    public GameObject txx, txxx, txxxx, tx100;
    public Rigidbody2D Rb;
    public GameObject player;

    private playercontrols pc;
    public bool frozen;
    public Animator Anim;
    // Start is called before the first frame update
    void Start()
    {
        Rb = player.GetComponent<Rigidbody2D>();
        Anim = player.GetComponent<Animator>();
        pc = FindObjectOfType<playercontrols>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            StartCoroutine("Talk");
            if (!frozen)
            {
                Rb.constraints = RigidbodyConstraints2D.FreezeAll;
                StartCoroutine("Spine");
                frozen = true;
                if ( !pc.holdingtorchinhand)
                {
                    Anim.CrossFade("Idle without crystal", 0f);
                }
                else
                {
                    Anim.CrossFade("idle with crystal", 0f);
                }
            }
        }
    }
    IEnumerator Talk()
    {
        tx100.SetActive(true);
        txx.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        Anim.enabled = false;
        yield return new WaitForSeconds(0.4f);
        yield return new WaitForSeconds(0.5f);
        txxx.SetActive(true);
        frozen = false;
        //yield return new WaitForSeconds(2.5f);
        //tx.SetActive(false);
        //txx.SetActive(false);
        //txxx.SetActive(false);
        txxxx.SetActive(false);

    }
    IEnumerator Spine()
    {
        yield return new WaitForSeconds(1f);
        Rb.constraints = RigidbodyConstraints2D.None;
        Anim.enabled = true;
    }
}

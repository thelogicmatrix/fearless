﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveText : MonoBehaviour
{
    public GameObject txx, txxx, tx100;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            txx.SetActive(false);
            txxx.SetActive(false);
            tx100.SetActive(false);
        }
    }
}

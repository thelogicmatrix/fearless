﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Keytext : MonoBehaviour
{
    private playercontrols pc;
    // Start is called before the first frame update
    void Start()
    {
        pc = FindObjectOfType<playercontrols>();
    }

    // Update is called once per frame
    void Update()
    {
        if (pc.holdingkeyinhand)
        {
            Destroy(gameObject);
        }
    }
}

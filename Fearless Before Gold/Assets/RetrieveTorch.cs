﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class RetrieveTorch : MonoBehaviour
{
    //public GameObject Torch;
    public bool touching;
    public GameObject unlitlamp;
    private playercontrols pc;
    private keybehaviour kb;
    // Start is called before the first frame update
    void Start()
    {
        pc = FindObjectOfType<playercontrols>();
        kb = FindObjectOfType<keybehaviour>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            touching = true;
        pc.ifLitLampTouch = true;
    }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            touching = false;
        pc.ifLitLampTouch = false;
    }
    }

    // Update is called once per frame
    void Update()
    {
        {
            if (pc.sleep && touching)
                //&& pc.isGrounded && touching && !kb.keytouching &&!pc.holdingkeyinhand &&!pc.holdingtorchinhand) //&&pc.litlamptouch
            {
                print("delight");
               // pc.ifLitLampTouch = false;
                //pc.PlayerUnlit = true;
                unlitlamp.SetActive(true);
                gameObject.SetActive(false);
                //StartCoroutine("Return");
            }
        }
    }
    IEnumerator Return()
    {
        yield return new WaitForSeconds(0.001f);
            touching = false;
            pc.ifLitLampTouch = false;
        pc.PlayerUnlit = true;
            unlitlamp.SetActive(true);
            gameObject.SetActive(false);
        //}
    }
}

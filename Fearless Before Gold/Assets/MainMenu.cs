﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.CrossPlatformInput;

public class MainMenu : MonoBehaviour
{
    public string leveltoload;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if ((CrossPlatformInputManager.GetButtonDown(("Back"))))
        {
            SceneManager.LoadScene(leveltoload);
        }
    }
}
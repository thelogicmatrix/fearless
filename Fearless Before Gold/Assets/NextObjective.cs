﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextObjective : MonoBehaviour
{
    public GameObject tx;
    public GameObject txx, txxx, txxxx, txxxxx, txxxxxx, tx8, tx9, tx10, tx11, tx1, tx2, tx3, tx4, tx100, tx101;
    private playercontrols pc;
    public bool Next = false;
    public bool next = false;
    // Start is called before the first frame update
    void Start()
    {
        pc = FindObjectOfType<playercontrols>();
        StartCoroutine("Warning");
    }

    // Update is called once per frame
    void Update()
    {
      //if (!Next)
      //{
      //    tx.SetActive(true);
       //   StartCoroutine("NexObjective");
       //   Next = true;
     // }
        if (pc.holdingtorchinhand &!Next)
        {
            txx.SetActive(false);
            tx.SetActive(true);
            StartCoroutine("NexObjective");
            tx4.SetActive(true);
            tx100.SetActive(false);
            Next = true;
        }
        if (pc.holdingkeyinhand & !next)
        {
          txxxx.SetActive(false);
        txxxxx.SetActive(true);
            tx8.SetActive(false);
            tx9.SetActive(false);
            tx10.SetActive(false);
            tx11.SetActive(false);
            tx101.SetActive(false);
            StartCoroutine("NxObjective");
            next = true;
        }
    }

    IEnumerator Warning()
    {
        tx3.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        tx1.SetActive(true);
        yield return new WaitForSeconds(0.4f);
        tx2.SetActive(true);
    }

    IEnumerator NexObjective()
        {
            yield return new WaitForSeconds(1f);
            tx.SetActive(false);
        txxx.SetActive(true);
        }
    IEnumerator NxObjective()
    {
        yield return new WaitForSeconds(1f);
    txxxxx.SetActive(false);
    txxxxxx.SetActive(true);
    }

}

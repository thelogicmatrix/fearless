﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.CrossPlatformInput;

public class ENDGAME : MonoBehaviour
{
    
    
    //sound variable/source
    public AudioSource openSound;

    //level loading variable
    //public string leveltoload;

    //door sprite variable
    public Sprite dooropen;
    public Sprite doorclose;
    public SpriteRenderer door;
    public Text tx;
    public GameObject txxx, txxxx;

    //door unlocked variable
    public bool doorunlocked;
    public bool goodtouch = false;
    public bool sound = false;

    // Start is called before the first frame update
    void Start()
    {
        doorunlocked = false;
    }

    // Update is called once per frame
    void Update()
    {


        //door changing sprites
        if (doorunlocked == true)
        {
            door.sprite = dooropen;
        }
        else if (doorunlocked == false)
        {
            door.sprite = doorclose;
        }
           


    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Key" && !sound)
        {
            openSound.Play();
            print("Win");
            doorunlocked = true;
            sound = true;
            Destroy(tx);
            txxx.SetActive(true);
            txxxx.SetActive(true);

        }
        if (other.tag == "Player")
        {
            goodtouch = true;
        }
    }
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            goodtouch = true;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            goodtouch = false;
        }
    }


    //void OnTriggerStay2D(Collider2D other)
    // {

    // if (other.tag == "Player")
    // {
    //  if (CrossPlatformInputManager.GetButtonDown("Jump") && doorunlocked)
    //  {
    //  SceneManager.LoadScene(leveltoload);
    //  print("finishlevel");
    // }
    //  }
    //  }

}

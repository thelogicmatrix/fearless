﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

    //what/who to follow variables for camera
    public GameObject target; //define target for camera to follow
    public float followAhead; //following action for camera I.E. camera following player to left when player moves left
    private Vector3 targetPosition; //variable for targetposition
    public bool followtarget; //this checks if the camera is following the set target

    //variables for smoothing of camera/jittery removal
    public float smoothing; //smoothing of camera flow, so that camera is not jittery


     
	// Use this for initialization
	void Start ()
    {
		
	}

    // Update is called once per frame
    void Update()
    {
        if (followtarget)
        {
            Vector3 targetPosition = new Vector3(target.transform.position.x, target.transform.position.y, transform.position.z); //allowing camera to move command line

            if (target.transform.localScale.x > 0.0f) //when player moves right
            {
                targetPosition = new Vector3(targetPosition.x + followAhead, targetPosition.y, targetPosition.z);
            }
            else //when player moves left
            {
                targetPosition = new Vector3(targetPosition.x - followAhead, targetPosition.y, targetPosition.z);
            }

            transform.position = Vector3.Lerp(transform.position, targetPosition, smoothing * Time.deltaTime);
        }

        
    }
    
}

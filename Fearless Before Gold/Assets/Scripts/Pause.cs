﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.CrossPlatformInput;

public class Pause : MonoBehaviour
{
    public static bool Gameispaused = false;
    public GameObject PausemenuUI;
    public GameObject NormalUI;
    // public string leveltoload;
    // Update is called once per frame
    void Update()
    {
        if ((CrossPlatformInputManager.GetButtonDown(("Pause"))))
        {

            //if (Gameispaused)
            //  {
            //  Resume();
            //  //   }
            //  else
            // {
            ToggleStopgame();
            //  }



            //SceneManager.LoadScene(leveltoload);
        }
    }


    void Resume()
    {
        NormalUI.SetActive(true);
        PausemenuUI.SetActive(false);
        Time.timeScale = 1f;
        Gameispaused = false;

    }


    public void ToggleStopgame()
    {
        if (Time.timeScale > 0f)
        {
            NormalUI.SetActive(false);
            PausemenuUI.SetActive(true);
            Time.timeScale = 0f;
            Gameispaused = true;
        }
        else
        {
            NormalUI.SetActive(true);
            PausemenuUI.SetActive(false);
            Time.timeScale = 1f;
            Gameispaused = false;
        }
    }
}

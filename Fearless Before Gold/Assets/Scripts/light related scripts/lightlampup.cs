﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class lightlampup : MonoBehaviour
{
    public bool correctlamp;
    public GameObject litlamp;
    private playercontrols pc;
    private keybehaviour kb;
    // Start is called before the first frame update
    void Start()
    {
        pc = FindObjectOfType<playercontrols>();
    }

    // Update is called once per frame
    void Update()
    {
        {
            if (pc.awaken && correctlamp)
                //pc.isGrounded && correctlamp && pc.holdingtorchinhand)
            {
                //StartCoroutine("Delayed");
                print("light");
                //pc.PlayerUnlit = false;
                //pc.ifLitLampTouch = true;
                litlamp.SetActive(true);
                gameObject.SetActive(false);
            }
        }
    }
  void OnTriggerEnter2D(Collider2D other)
  {
        //if (other.tag == "Torch")
            //correctlamp = true;
        if (other.tag == "Player")
        {
            pc.PlayerUnlit = true;
            correctlamp = true;
        }
  }
  void OnTriggerExit2D(Collider2D other)
  {
        //if (other.tag == "Torch")
            //correctlamp = false;
        if (other.tag == "Player")
        {
            pc.PlayerUnlit = false;
            correctlamp = false;
        }
  }
    IEnumerator Delayed()
    {
        yield return new WaitForSeconds(0.1f);
        litlamp.SetActive(true);
        gameObject.SetActive(false);
    }
}

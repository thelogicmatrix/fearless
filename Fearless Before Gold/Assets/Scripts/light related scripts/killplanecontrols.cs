﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class killplanecontrols : MonoBehaviour
{
    private playercontrols pc;
    // Start is called before the first frame update
    void Start()
    {
        pc = FindObjectOfType<playercontrols>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            other.gameObject.SetActive(false);
            pc.death = true;
        }
    }
}

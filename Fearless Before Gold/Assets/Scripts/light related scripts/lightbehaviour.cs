﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lightbehaviour : MonoBehaviour
{

    private playercontrols pc;
    // Start is called before the first frame update
    void Start()
    {
        pc = FindObjectOfType<playercontrols>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            pc.timebeforedeath = 3.0f;
            print("Player is carrying light");
        }
    }
}

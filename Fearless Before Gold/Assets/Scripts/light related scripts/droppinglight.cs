﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class droppinglight : MonoBehaviour
{
    public GameObject torch;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Drop"))
        {
            //gameObject.AddComponent(typeof(Rigidbody2D));
            Instantiate(torch, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}

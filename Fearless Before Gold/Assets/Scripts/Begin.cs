﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.UI;

public class Begin : MonoBehaviour
{
    void OnAwake()
    {
        StartCoroutine(FadeTextToZeroAlpha(1f, GetComponent<Image>()));
    }
    void Start()
    {
      StartCoroutine(FadeTextToFullAlpha(1f, GetComponent<Image>()));
}
    public void Update()
    {
        if (CrossPlatformInputManager.GetButtonDown("Drop"))
        {
            startgame();
        }
    }

    public void startgame()
    {
        SceneManager.LoadScene("Prologue");
    }

    public IEnumerator FadeTextToZeroAlpha(float t, Image i)
    {
        i.color = new Color(i.color.r, i.color.g, i.color.b, 1);
        while (i.color.a > 0.0f)
        {
            i.color = new Color(i.color.r, i.color.g, i.color.b, i.color.a - (Time.deltaTime / t));
            yield return null;
        }
    }
    public IEnumerator FadeTextToFullAlpha(float t, Image i)
    {
        i.color = new Color(i.color.r, i.color.g, i.color.b, 0);
        while (i.color.a < 1.0f)
        {
            i.color = new Color(i.color.r, i.color.g, i.color.b, i.color.a + (Time.deltaTime / t));
            yield return null;
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.UI;

public class keybehaviour : MonoBehaviour
{
    public GameObject Key;
    public GameObject player;
    public bool keytouching, dropDelay;
    public Vector3 ObjectOffset;
    private playercontrols pc;
    //private ParticleSystemRenderer psr;
    //public Vector3 dropObjectOffset;
    // Start is called before the first frame update
    private SpriteRenderer sr;
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        pc = FindObjectOfType<playercontrols>();
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            keytouching = true;
        }
        if (other.tag == "Ground")
        {
            Destroy(GetComponent<Rigidbody2D>());
            print("boop");
        }
    }
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            keytouching = true;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            keytouching = false;
        }
    }

    // Update is called once per frame
    public void Update()
    {
        



        if (CrossPlatformInputManager.GetButtonDown("Drop"))
         {
            
            if (pc.holdingkeyinhand)
            {
                keytouching = true;
                //transform.position = player.transform.position +dropObjectOffset;
                gameObject.AddComponent(typeof(Rigidbody2D));
                DetachFromParent();
                print("Key Drop");

            }
            else if (keytouching && !pc.holdingkeyinhand && !pc.holdingtorchinhand && !dropDelay)
            {
                print("Key Pickup");
                if (pc.facingRight)
                {
                    
                    transform.position = player.transform.position + ObjectOffset;
                    Key.transform.parent = player.transform;
                    sr.flipY = false;
                    dropDelay = true;
                    StartCoroutine("DropDelay");
                }
                else
                {
                    
                    transform.position = player.transform.position + -ObjectOffset;
                    Key.transform.parent = player.transform;
                    sr.flipY = true;
                    dropDelay = true;
                    StartCoroutine("DropDelay");
                }
            }
            
        }
    }
    public void DetachFromParent()
    {
        transform.parent = null;
    }
    
    IEnumerator DropDelay()
    {
        yield return new WaitForSeconds(1.5f);
        dropDelay = false;
    }
    
}
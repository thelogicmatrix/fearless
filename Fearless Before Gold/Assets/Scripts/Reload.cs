﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class Reload : MonoBehaviour
{
    public AudioSource ambienceSound;
    private playercontrols pc;
    private RetrieveTorch rt;
    private Droppable dp;
    //public Transform torch;
    public Transform player;
    public bool inactivetorch;
    public GameObject x, xx, xxx;
    public bool once = true;
    // Start is called before the first frame update
    void Start()
    {
        pc = FindObjectOfType<playercontrols>();
        rt = FindObjectOfType<RetrieveTorch>();
        dp = FindObjectOfType<Droppable>();
    }

    // Update is called once per frame
    void Update()
    {
        //if (torch.transform.IsChildOf(levelmanager))
        //{
           // inactivetorch = true;

       // }
       // else inactivetorch = false;
        if (pc.death && once)
        {
            StartCoroutine("End");
            once = false;
        }
            //if (CrossPlatformInputManager.GetButton("Drop") && inactivetorch && rt.touching)
            //{
            //torch.gameObject.SetActive(true);
            //torch.transform.parent = null;
            //torch.transform.position = player.transform.position;
            //torch.gameObject.transform.parent = player.transform;
            //dp.torchtouching = true;
            // inactivetorch = false;
            // print("pickup");
            // }
        }
    IEnumerator End()
    {
        ambienceSound.Stop();
        x.SetActive(true);
        xx.SetActive(false);
        xxx.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        x.SetActive(false);
        yield return new WaitForSeconds(2f);
        Scene scene = SceneManager.GetActiveScene(); SceneManager.LoadScene(scene.name);
    }
}

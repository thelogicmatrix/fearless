﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[System.Serializable]
public class Dialogue 
{
    //this object will be used to pass dialogues into dialogue manager

    public string name; //for name/item of sentence/speaker

    [TextArea(3,10)] //allows editing of strings/sentences in unity itself. Numbers specify min and max number of sentences
    public string[] sentences; //for sentences strings
    





}

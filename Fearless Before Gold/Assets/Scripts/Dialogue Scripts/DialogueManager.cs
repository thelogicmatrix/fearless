﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DialogueManager : MonoBehaviour
{
    private Queue<string> sentences;   //keeps track of all sentences to be displayed at different times during dialogue


    public Text nameText;
    public Text dialogueText;

    // Start is called before the first frame update
    void Start()
    {
        sentences = new Queue<string>(); //queues all sentences/strings to be displayed



    }

   
    public void StartDialogue (Dialogue dialogue)
    {

        

        sentences.Clear(); //clears sentences when dialogue starts

        nameText.text = dialogue.name;

        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence); //
        }


        DisplaynextSentence();

    }

    public void DisplaynextSentence()
    {
        if (sentences.Count == 0) //when no more sentences are left
        {
            EndDialogue();
            return;
        }

        string sentence = sentences.Dequeue();
        dialogueText.text = sentence;



    }

    void EndDialogue()
    {
        SceneManager.LoadScene("Tutorial1");
    }


}

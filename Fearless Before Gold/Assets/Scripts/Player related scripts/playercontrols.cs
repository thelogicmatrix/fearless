﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.SceneManagement;

public class playercontrols : MonoBehaviour
{
    //animation variables
    private Animator myanim;
    private Rigidbody2D rb2D;

    //torch variables
    public bool holdingtorchinhand, holdingtorchinhand1, holdingtorchinhand2, holdingtorchinhand3, holdingtorchinhand4, holdingtorchinhand5, holdingtorchinhand6, holdingtorchinhand7;
    public Transform player;
    public Transform torch, torch1, torch2, torch3, torch4, torch5, torch6;
    public Transform Key;
    public bool holdingkeyinhand;
    public bool death;
    public bool awaken, sleep;
    public float timebeforedeath;
    public bool inlightrange;
    public bool ifLitLampTouch;
    public bool PlayerUnlit;
    public string leveltoload;
    //public bool walk;

    //calling other scripts
    private Droppable other;
    private ENDGAME eg;
    //private buttonscript bs;


    //variables for body
    Rigidbody2D rb; //rigidbody variable
    SpriteRenderer sr; //spriterenderer variable

    //movement variable
    public bool facingRight; //for sprite directional face
    public float walkingspeed; //movement speed of character
    private float facingdirection; //facing direction


    //jumping items
    private int jumpcount; //number of jumps
    public int jumpcountvalue; //the value of jump
    public float jumpspeed; //jump speed


    //ground check variables
    public Transform groundCheck; //ground check to prevent infinite jump
    public float groundCheckRadius; //whether player is within proximity of defined ground items
    public LayerMask realGround; //variable to identify  which layer in unity is enabled
    public bool isGrounded; //check if player is grounded


    //sound variables
    public AudioSource jumpSound;
    public AudioSource dangerSound;
    public AudioSource walkSound;
    public AudioSource deathSound;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>(); //getting rigidbody component
        eg = FindObjectOfType<ENDGAME>();
        sr = GetComponent<SpriteRenderer>(); //sprite rendering in game component
        timebeforedeath = 0.1f;
        death = false;
        facingRight = true;
        //torch = GameObject.Find("Torch(Clone)").transform;
        myanim = GetComponent<Animator>();
        rb2D = GetComponent<Rigidbody2D>();
        //bs = GetComponent<buttonscript>();
    }

    // Update is called once per frame
    void Update()
    {
        //animator control variable changer
        myanim.SetFloat("Speed", Mathf.Abs(rb.velocity.x)); //to follow whether player is moving and allow animator to change value of speed variable
        myanim.SetBool("Ground", isGrounded); // allows animator control to check if player is grounded and change boolean variable of grounded
        //timebeforedeath -= Time.deltaTime;
        if (!inlightrange)
        {
            timebeforedeath -= Time.deltaTime;
        }
        if (timebeforedeath <= 0)
        {
            Destroyseika();
        }
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, realGround); // basically creates a circle that checks for variables in circle
                                                                                                   //if (Mathf.Abs(rb.velocity.x) > 0)
                                                                                                   //{
                                                                                                   // walk = true;
                                                                                                   //}
                                                                                                   //else walk = false;
                                                                                                   //if (walk)
                                                                                                   //{
                                                                                                   //walkSound.Play();
                                                                                                   //}
                                                                                                   //else walkSound.Stop();
        if (isGrounded)
        {

            //jumping code
            jumpcount = jumpcountvalue; //this makes the number of jumps equal to the jump count ie: double jump require 2 jump counts
            if ((CrossPlatformInputManager.GetButtonDown("Jump") && jumpcount > 0))
            {
                rb.velocity = new Vector2(rb.velocity.x, jumpspeed); // for jumping
                jumpcount -= 1;
                jumpSound.Play();
                print("Jump");
            }
            if (CrossPlatformInputManager.GetButtonDown("Jump") && eg.doorunlocked && eg.goodtouch)
            {
                SceneManager.LoadScene(leveltoload);
                print("finishlevel");
            }
        }

        facingdirection = CrossPlatformInputManager.GetAxisRaw("Horizontal");   //direction input
        //facing right and rightward movement
        if (facingdirection < 0)
        {
            rb.velocity = new Vector2(facingdirection * walkingspeed, rb.velocity.y);
            {
                transform.localScale = new Vector3(-Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
            }
            facingRight = false;

        }

        //facing left and leftward movement
        else if (facingdirection > 0)
        {
            rb.velocity = new Vector2(facingdirection * walkingspeed, rb.velocity.y);
            {
                transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
            }
            facingRight = true;

        }

        //key boolean check
        if (Key.transform.IsChildOf(player))
        {
            holdingkeyinhand = true;
        }
        else
        {
            holdingkeyinhand = false;
        }
        // holding all the torches
        if (holdingtorchinhand)
        {
            timebeforedeath = 0.1f;
        }
        if (torch.transform.IsChildOf(player))
        {
            holdingtorchinhand = true; //
            myanim.SetBool("torch in hand", true);
            holdingtorchinhand1 = true;
            holdingtorchinhand2 = false;
            holdingtorchinhand3 = false;
            holdingtorchinhand4 = false;
            holdingtorchinhand5 = false;
            holdingtorchinhand6 = false;
            holdingtorchinhand7 = false;

        }
        else if (torch1.transform.IsChildOf(player))
        {
            holdingtorchinhand = true; //
            myanim.SetBool("torch in hand", true);
            holdingtorchinhand2 = true;
            holdingtorchinhand1 = false;
            holdingtorchinhand3 = false;
            holdingtorchinhand4 = false;
            holdingtorchinhand5 = false;
            holdingtorchinhand6 = false;
            holdingtorchinhand7 = false;
        }
        else if (torch2.transform.IsChildOf(player))
        {
            holdingtorchinhand = true; //
            myanim.SetBool("torch in hand", true);
            holdingtorchinhand3 = true;
            holdingtorchinhand1 = false;
            holdingtorchinhand2 = false;
            holdingtorchinhand4 = false;
            holdingtorchinhand5 = false;
            holdingtorchinhand6 = false;
            holdingtorchinhand7 = false;

        }
        else if (torch3.transform.IsChildOf(player))
        {
            holdingtorchinhand = true; //
            myanim.SetBool("torch in hand", true);
            holdingtorchinhand4 = true;
            holdingtorchinhand1 = false;
            holdingtorchinhand2 = false;
            holdingtorchinhand3 = false;
            holdingtorchinhand5 = false;
            holdingtorchinhand6 = false;
            holdingtorchinhand7 = false;

        }
        else if (torch4.transform.IsChildOf(player))
        {
            holdingtorchinhand = true; //
            myanim.SetBool("torch in hand", true);
            holdingtorchinhand5 = true;
            holdingtorchinhand1 = false;
            holdingtorchinhand2 = false;
            holdingtorchinhand3 = false;
            holdingtorchinhand4 = false;
            holdingtorchinhand6 = false;
            holdingtorchinhand7 = false;

        }
        else if (torch5.transform.IsChildOf(player))
        {
            holdingtorchinhand = true; //
            myanim.SetBool("torch in hand", true);
            holdingtorchinhand6 = true;
            holdingtorchinhand1 = false;
            holdingtorchinhand2 = false;
            holdingtorchinhand3 = false;
            holdingtorchinhand4 = false;
            holdingtorchinhand5 = false;
            holdingtorchinhand7 = false;

        }
        else if (torch6.transform.IsChildOf(player))
        {
            holdingtorchinhand = true; //
            myanim.SetBool("torch in hand", true);
            holdingtorchinhand7 = true;
            holdingtorchinhand1 = false;
            holdingtorchinhand2 = false;
            holdingtorchinhand3 = false;
            holdingtorchinhand4 = false;
            holdingtorchinhand5 = false;
            holdingtorchinhand6 = false;

        }
        else
        {
            holdingtorchinhand = false;
            myanim.SetBool("torch in hand", false);
            holdingtorchinhand1 = false;
            holdingtorchinhand2 = false;
            holdingtorchinhand3 = false;
            holdingtorchinhand4 = false;
            holdingtorchinhand5 = false;
            holdingtorchinhand6 = false;
            holdingtorchinhand7 = false;
        }

            if (holdingtorchinhand)
            {
                timebeforedeath = 0.1f;
                inlightrange = true;
            }
        //if (CrossPlatformInputManager.GetButtonDown("Drop") && isGrounded && ifLitLampTouch)
       // {
         //  StartCoroutine
        //}


            //  if (key.transform.IsChildOf(player))
            //  {
            //holdingkeyinhand = true;

            // }
            //   else
            //  {
            //   holdingkeyinhand = false;
            //    holdingiteminhand = true;
            // }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "light")
            {
                timebeforedeath = 0.1f;
                inlightrange = true;
            }
            // if (other.tag == "Text")
            //{
            //if (!frozen)
            //{
            //    rb2D.constraints = RigidbodyConstraints2D.FreezeAll;
            //Destroy(GetComponent<Rigidbody2D>());
            //   StartCoroutine("Spine");
            //  frozen = true;
            // }
            // }
        }

        private void OnTriggerStay2D(Collider2D other)
        {
            if (other.tag == "light")
            {
                timebeforedeath = 0.1f;
                inlightrange = true;
            }
       //   if (other.tag == "Lamp")
     // {
      //    ifTorchTouch = true;
     // }
        }
        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.tag == "light")
            {
                inlightrange = false;
            }
        //if (other.tag == "Lamp")
        //  {
        // ifTorchTouch = false;
        //}
    }
        public void Destroyseika()
        {
            death = true;
            //myanim.SetTrigger("Death");
            StartCoroutine("Dead");
        deathSound.Play();


        //gameObject.SetActive(false);
        //SceneManager.LoadScene("Fearless");
    }
        IEnumerator Dead()
        {
            //  rb.constraints = RigidbodyConstraints2D.FreezeAll;
            //Destroy(GetComponent<Rigidbody2D>());
            yield return new WaitForSeconds(0.5f);
            myanim.SetTrigger("Death");
            yield return new WaitForSeconds(1f);
            gameObject.SetActive(false);
        }
        //IEnumerator Spine()
        //{
        //yield return new WaitForSeconds(3f);
        //gameObject.AddComponent(typeof(Rigidbody2D));
        //rb2D.constraints = RigidbodyConstraints2D.None;
        // frozen = false;
        //}
    }

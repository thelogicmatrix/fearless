﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoldingTorchChild : MonoBehaviour
{
    public Transform torch, lightradius;
    public Transform levelmanager;
    public bool torchinlamp;
    public bool lightradiusinlamp;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (torch.transform.IsChildOf(levelmanager))
        {
          torchinlamp = true;
        }
      else torchinlamp = false;
        if (lightradius.transform.IsChildOf(levelmanager))
        {
            lightradiusinlamp = true;
        }
        else lightradiusinlamp = false;
    }
}

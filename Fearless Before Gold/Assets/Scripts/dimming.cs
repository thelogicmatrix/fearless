﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dimming : MonoBehaviour
{
    //public GameObject light;
    private playercontrols pc;
    public float duration = 1.0f;
    Color color0 = Color.white;
    Color color1 = Color.grey;
    private bool firsttime;
    Light yagami;
    //public AudioSource spawnSound;
    // Start is called before the first frame update
    void Start()
    {
        pc = FindObjectOfType<playercontrols>();
        //spawnSound.Play();
        yagami = GetComponentInChildren<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        float t = Mathf.PingPong(Time.time, duration) / duration;
        yagami.color = Color.Lerp(color0, color1, t);
        if (pc.death && firsttime)
        {
            StartCoroutine("Flicker");
            firsttime = false;
        }
    }
    IEnumerator Flicker()
    {
        yield return new WaitForSeconds(0.5f);
        transform.GetChild(0).gameObject.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        transform.GetChild(0).gameObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        transform.GetChild(0).gameObject.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        transform.GetChild(0).gameObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        transform.GetChild(0).gameObject.SetActive(false);
    }
}

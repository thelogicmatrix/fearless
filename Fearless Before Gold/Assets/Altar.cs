﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.CrossPlatformInput;

public class Altar : MonoBehaviour
{
    public Sprite Altaractivated;
    public Sprite AltarDeactivated;
    public SpriteRenderer altarsprite;
    public GameObject flash;
    public GameObject lightedbg;
    public bool activated;
    public bool goodtouch = false;
    // Start is called before the first frame update
    void Start()
    {
        activated = false;




    }

    // Update is called once per frame
    void Update()
    {
        //altar changing sprites
        if (activated == true)
        {
            altarsprite.sprite = Altaractivated;
        }
        else if (Altaractivated == false)
        {
            altarsprite.sprite = AltarDeactivated;
        }




    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Torch")
        {
            activated = true;
            StartCoroutine("Saviour");
           // if (CrossPlatformInputManager.GetButtonDown("Jump") && goodtouch &&activated)
           // {
             //   SceneManager.LoadScene("Epilogue");
         //   }
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            goodtouch = true;
        }


    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            goodtouch = false;
        }
    }
    IEnumerator Saviour()
    {
        yield return new WaitForSeconds(0.5f);
        flash.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        flash.SetActive(false);
        lightedbg.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        flash.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("Epilogue");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InitiateWords : MonoBehaviour
{
    public GameObject txx, txxx, txxxx, txxxxx, txxxxxx, txxxxxxx, tx100;
    public GameObject player;
    public Rigidbody2D Rb;
    public bool frozen;
    public Animator Anim;

    private playercontrols pc;
    // Start is called before the first frame update
    void Start()
    {
        Rb = player.GetComponent<Rigidbody2D>();
        Anim = player.GetComponent<Animator>();
        pc = FindObjectOfType<playercontrols>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            StartCoroutine("Talk");
            if (!frozen)
            {
                Rb.constraints = RigidbodyConstraints2D.FreezeAll;
                StartCoroutine("Spine");
                frozen = true;
                //Rb.velocity = Vector2.zero;
                if (!pc.holdingtorchinhand)
                {
                    Anim.CrossFade("Idle without crystal", 0f);
                }
                else
                {
                    Anim.CrossFade("idle with crystal", 0f);
                }
                //Anim.SetTrigger("Freeze");
            }
        }
    }
    IEnumerator Talk()
    {
        tx100.SetActive(true);
        txxxxx.SetActive(false);
        txxxxxx.SetActive(true);
        txx.SetActive(true);
        yield return new WaitForSeconds(0.15f);
        Anim.enabled = false;
        yield return new WaitForSeconds(0.35f);
        yield return new WaitForSeconds(0.5f);
        txxx.SetActive(true);
        frozen = false;
        //yield return new WaitForSeconds(0.1f);
        txxxx.SetActive(false);
        txxxxxx.SetActive(false);
        txxxxxxx.SetActive(true);
    }
    IEnumerator Spine()
    {
        yield return new WaitForSeconds(1f);
        Rb.constraints = RigidbodyConstraints2D.None;
        Anim.enabled = true;
    }
}

